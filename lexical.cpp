#include "lexical.h"

bool Lexical::isDigit(char c) {
    int ascii = (int)c;
    if(ascii>=48 && ascii<=57) {
        return true;
    }
    return false;
}
bool Lexical::isLetterMayus(char c) {
    int ascii = (int)c;
    if(ascii>=65 && ascii<=90) {
        return true;
    }
    return false;
}
bool Lexical:: isLetterMinus(char c) {
    int ascii = (int)c;
    if(ascii>=97 && ascii<=122) {
       return true;
    }
    return false;
}
bool Lexical::isWhiteSpace(char c) {
    return (int)c == 32;
}
bool Lexical::isLetter(char c) {
    if(isLetterMayus(c)) {
        return true;
    }
    return isLetterMinus(c);
}
bool Lexical::isLeftUnaryOperator(char c) {
    if(c==Token::MINUS || c==Token::PLUS) {
        return true;
    }
    return false;
}
bool Lexical::isBasicOperator(char c) {
    if(Lexical::isLeftUnaryOperator(c) || c==Token::MULT || c==Token::DIV) {
        return true;
    }
    return false;
}
bool Lexical::isBinaryOperator(char c) {
    if(Lexical::isBasicOperator(c) || c==Token::POW) {
        return true;
    }
    return false;
}
bool Lexical::isOperator(char c) {
    if(Lexical::isBinaryOperator(c) || c==Token::FACT) {
        return true;
    }
    return false;
}
bool Lexical::isString(string x) {
    for(int i=0;i <x.size(); i++) {
        if(!isLetter(x.at(i))) {
            return false;
        }
    }
    return true;
}
bool Lexical::isInteger(string x) {
    for(int i=0; i<x.size(); i++) {
        if(!isDigit(x.at(i))) {
            return false;
        }
    }
    return true;
}
bool Lexical::isReal(string x) {
    int points = 0, position = -1;
    for(int i=0; i<x.size(); i++) {
        if(x.at(i)=='.') {
            points++; position = i;
        }
    }
    if(points>1) {
        return false;
    }
    return (isInteger(x.substr(0,position-1)) &&
            isInteger(x.substr(position+1, x.size())));
}
bool Lexical::isNumber(string x)
{
    if(isInteger(x)) {
        return true;
    }
    return isReal(x);
}
bool isFunction(string x) {
    int position = {-1};
    for(int i=0; i<x.size(); i++) {
        if(x.at(i)=='(') {
            position = i;
        }
    }
    if(!x.at(x.size())==')') {
        return false;
    }
    string name = x.substr(0, position);
    //incomplete
}

string Lexical::subString(string x, int i, int n) {
    string aux = "";
    for(int j=i; j<=n; j++) {
        aux += x.at(j);
    }
    return aux;
}

string Lexical::replaceString(string x,int i, int n, string r ) {
    if(i==0 && n==x.size()-1) {
        return  r;
    }
    else if(i==0 && n<x.size()-1) {
        return r+subString(x, n + 1, x.size()-1);
    }
    else if(i>0 && n==x.size()-1) {
        return subString(x, 0, i-1) + r;
    }
    else {
        return subString(x, 0, i-1) + r + subString(x, n+1, x.size()-1);
    }
}

