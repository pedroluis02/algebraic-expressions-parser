#ifndef TOKEN_H
#define	TOKEN_H

#include <string>
using namespace std;

class Token {
public:
    static const int FACT = '!';
    static const int POW = '^';
    static const int MULT =  '*';
    static const int DIV = '/';
    static const int PLUS = '+';
    static const int MINUS = '-';
    static const int PARENTHESES_BEGIN = '(';
    static const int PARENTHESES_END = ')';
    static const int SIMBOL = 1;
    static const int NUMBER = 2;
    
    static string lexema(int token_id);
};

#endif	/* TOKEN_H */

