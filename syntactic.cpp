#include "syntactic.h"

string Syntactic::removeWhiteSpaces(string x) {
    int it = 0; string aux = "";
    while(it < x.size()) {
        if(!Lexical::isWhiteSpace(x.at(it))) {
           aux += x.at(it);
        }
        it++;
    }
    return aux;
}
int Syntactic::countDigits(int position, string x) {
    int pos = position;
    while(pos<x.size()) {
        if(Lexical::isDigit(x.at(pos))) {
            pos++;
        } else {
           break;
        }
    }
    return pos;
}
int Syntactic::parseExpression(string x) {
    if(x.empty()) {
       return -1;
    }
    int state = 0, position = 0;
    stack <char> my_stack; my_stack.push('T'); my_stack.push('Z');
    bool exp = true;
    while(position<x.size()) {
        char simbol = x.at(position);
        switch(state) {
            case 0:
                if(simbol==Token::MINUS) {
                    state = 4;
                }
                else if(Lexical::isDigit(simbol) && my_stack.top()=='Z') {
                    position = countDigits(position, x);
                    position--; my_stack.pop(); state = 2;
                }
                else if(Lexical::isLetter(simbol) && my_stack.top()=='Z') {
                    my_stack.pop(); state = 2;
                }
                else if(simbol==Token::PARENTHESES_BEGIN && my_stack.top()=='Z') {
                    state = 1; my_stack.pop(); my_stack.push('P');
                }
                else {
                    exp = false;
                }
                break;

            case 1:
                if(Lexical::isDigit(simbol)) {
                    position = countDigits(position, x);
                    position--; state = 2;
                }
                else if(Lexical::isLetter(simbol)) {
                    state = 2;
                }
                else if(simbol==Token::MINUS) {
                    my_stack.push('Z'); state = 4;
                }
                else if(simbol==Token::PARENTHESES_BEGIN && my_stack.top()=='P') {
                    my_stack.push('P'); state = 1;
                }
                else {
                    exp = false;
                }
                break;

            case 2:
                if(simbol==Token::PARENTHESES_END && my_stack.top()=='P') {
                    my_stack.pop(); state = 2;
                }
                else if(simbol==Token::FACT) {
                    state = 5;
                }
                else {
                    exp = Lexical::isBinaryOperator(simbol);
                    if(exp) {
                       state = 3; my_stack.push('Z');
                    }
                }
                break;

            case 3:
                if(Lexical::isLetter(simbol) && my_stack.top()=='Z') {
                     my_stack.pop(); state = 2;
                }
                else if(simbol==Token::PARENTHESES_BEGIN && my_stack.top()=='Z') {
                     my_stack.pop(); my_stack.push('P'); state = 1; 
                }
                else if(Lexical::isDigit(simbol) && my_stack.top()=='Z') {
                    position = countDigits(position, x);
                    position--; my_stack.pop(); state = 2;
                }
                else {
                    exp = false;
                }
                break;

            case 4:
                if(simbol==Token::PARENTHESES_BEGIN && my_stack.top()=='Z') {
                    my_stack.pop(); state = 1; my_stack.push('P');
                }
                else if(Lexical::isDigit(simbol) && my_stack.top()=='Z') {
                    position = countDigits(position, x);
                    position--; my_stack.pop(); state = 2;
                }
                else if(Lexical::isLetter(simbol) && my_stack.top()=='Z') {
                    my_stack.pop(); state = 2;
                }
                else exp = false;
                break;

            case 5:
                if(simbol==Token::PARENTHESES_END && my_stack.top()=='P') {
                    my_stack.pop(); state = 2;
                }
                else {
                    exp = Lexical::isBinaryOperator(simbol);
                    if(exp) {
                       state = 3; my_stack.push('Z');
                    }
                }
                break;
        }
        if(!exp) {
            break;
        }
        position++;
    }
    if((state==2 || state==5) && my_stack.top()=='T' && position==x.size()) {
        return position;
    }
    return position;
}
int Syntactic::mainOperator(string x)
{
    char op = '.'; int pos = -1; stack <char> my_stack;
    my_stack.push('T');
    for(int i=0; i<x.size(); i++) {
        char s = x.at(i);
        if(s==Token::PARENTHESES_BEGIN) {
            my_stack.push('P');
        }
        else if(s==Token::PARENTHESES_END && my_stack.top()=='P') {
           my_stack.pop();
        }
        else if(Lexical::isOperator(s) && op=='.' && my_stack.top()=='T') {
            op = s; pos = i;
        }
        else if(Lexical::isOperator(s) && op!='.' && my_stack.top()=='T') {
            switch(op) {
                case Token::MINUS :
                    if(pos==0 && (s==Token::PLUS || s==Token::MINUS)) {
                        op = s; pos = i;
                    }
                    break;
                case Token::PLUS  :
                    if(s==Token::MINUS) {
                        op = s; pos = i;
                    }
                    else if(s==Token::PLUS) {
                        pos = i;
                    }
                    break;
                case Token::MULT  :
                    if(s==Token::MINUS || s==Token::PLUS) {
                        op = s; pos = i;
                    }
                    else if(s==Token::PLUS) {
                        pos = i;
                    }
                    break;
                case Token::DIV   :
                    if(s==Token::PLUS || s==Token::MINUS || s==Token::MULT) {
                        op = s; pos = i;
                    }
                    else if(s==Token::DIV) {
                        pos = i;
                    }
                    break;
                case Token::POW   :
                    if(Lexical::isBasicOperator(s)) {
                        op = s; pos = i;
                    }
                    else if(s==Token::POW) {
                        pos = i;
                    }
                    break;
                case Token::FACT  :
                    if(Lexical::isBinaryOperator(s)) {
                        op = s; pos = i;
                    }
                    break;
            }
        }
    }
    return pos;
}
string Syntactic::quitParentheseFirst(string x) {
    int op = mainOperator(x);
    if(op==-1) {
        if(x.at(0)==Token::PARENTHESES_BEGIN &&  
                x.at(x.size()-1)==Token::PARENTHESES_END) {
            x = Lexical::subString(x, 1, x.size()-2);
        }
    }
    return x;
}
string Syntactic::toPrefixExpression(string x) {
    string aux = quitParentheseFirst(x);
    int pos = mainOperator(aux);
    if(pos==-1) {
        return aux;
    }
    else {
       string x1 = Lexical::subString(aux, 0, pos-1),
              x2 = Lexical::subString(aux, pos+1, aux.size()-1),
              op = Lexical::subString(aux, pos, pos);
       if(pos==0 && op.at(0)==Token::MINUS) {
           return op+"(" + toPrefixExpression(
                   Lexical::subString(aux, pos+1, aux.size()-1)) +")";
       }
       else if(pos==aux.size()-1 && op.at(0)==Token::FACT) {
           return op + "(" + toPrefixExpression(
                   Lexical::subString(aux,0, aux.size()-2)) + ")";
       }
       else {
           return op + "(" + toPrefixExpression(x1) + ", " +
                   toPrefixExpression(x2) + ")";
       }
    }
}
string Syntactic::replaceParentheses(string x,string r) {
    stack <int> pila; pila.push(-1);int it = 0;
    for(;it<x.size();) {
        if(x.at(it)==Token::PARENTHESES_BEGIN) {
            pila.push(it);
        }
        else if(x.at(it)==Token::PARENTHESES_END && pila.top()!=-1) {
           x = Lexical::replaceString(x, pila.top(), it,"X");
           pila.pop(); it = pila.top();
        }
        it++;
    }
    return x;
}