
# Algebraic Expressions Parser
Console parser example for Alegraic Expression InFix

* Parse algebraic expression infix.
* Get main operator and position.
* Convert expression to prefix.

#### Input example:
```
(-4443!+1) + ((3*c+4!) - (23!+2^2^2))! + (a+32^2)
```

#### Output example:
```
Expression : (-4443!+1)+((3*c+4!)-(23!+2^2^2))!+(a+32^2) correct... 


 Main Operator... 

         Operator: +

         Position: 34


 Prefix Expression... 

         +(+(+(-(!(4443)), 1), !(-(+(*(3, c), !(4)), +(!(23), ^(^(2, 2), 2))))), +(a, ^(32, 2)))

```
