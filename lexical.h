#ifndef LEXICAL_H
#define	LEXICAL_H

#include "token.h"

class Lexical {
    //mains
public:
    static bool isDigit(char c);
    static bool isLetterMayus(char c);
    static bool isLetterMinus(char c);
    static bool isWhiteSpace(char c);
    static bool isLetter(char c);
    static bool isLeftUnaryOperator(char c);
    static bool isBasicOperator(char c);
    static bool isBinaryOperator(char c);
    static bool isOperator(char c);
    static bool isString(string x);
    static bool isInteger(string x);
    static bool isReal(string x);
    static bool isNumber(string x);
    static bool isFunction(string x);
    //utils
    static string subString(string x, int i, int n);
    static string replaceString(string x,int i, int n, string r );
};

#endif	/* LEXICAL_H */

