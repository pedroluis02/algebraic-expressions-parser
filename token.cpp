#include "token.h"
string Token::lexema(int token_id) {
    string dif = "Not defined";
    switch(token_id) {
        case FACT: dif = "Operator factorial"; break;
        case POW: dif = "Operator pow"; break;
        case MULT: dif = "Operator product"; break;
        case DIV: dif = "Operator division"; break;
        case PLUS: dif = "Operator add"; break;
        case MINUS: dif = "Operator subtraction"; break;
        case PARENTHESES_BEGIN: dif = "Parentheses begin"; break;
        case PARENTHESES_END: dif = "Parentheses end"; break;
        case NUMBER: dif = "Number"; break;
        case SIMBOL: dif = "Simbol"; break;
    }
    return dif;
}
