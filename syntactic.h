#ifndef SYNTACTIC_H
#define	SYNTACTIC_H

#include <stack>
#include "lexical.h"

class Syntactic
{
public:
    int parseExpression(string x);
    int mainOperator(string x);
    string toPrefixExpression(string x);
    string quitParentheseFirst(string x);
    string removeWhiteSpaces(string x);
    string replaceParentheses(string x,string r);
private:
    int countDigits(int position, string x);
};

#endif	/* SYNTACTIC_H */

