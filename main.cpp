#include <cstdlib>
#include <iostream>
#include "syntactic.h"

int main(int argc, char** argv)
{
    Syntactic syn; 
    string expr,aux;
    //string expr = lex.removeSpaces("(-4443!+1) + ((3*c+4!) - (23!+2^2^2))! + (a+32^2)");
    cout << "\n Input Expression: "; getline(cin, aux);
    expr = syn.removeWhiteSpaces(aux);
    int pos = syn.parseExpression(expr);
    if(pos==-1) {
        cout << "\n Input Expression.... " << endl << endl;
    }
    else if(pos<expr.size()) {
        cout << "\n Expression : " << expr << " incorrect... " << endl
             << "\n\t Stop in Character: " << expr.at(pos) << endl
             << "\n\t Stop in Position:  " << pos << endl << endl;
    }
    else if(pos==expr.size()) {
        cout << "\n Expression : " << expr << " correct... " << endl << endl;
        int p = syn.mainOperator(expr);
        cout << "\n Main Operator... " << endl;
        if(p==-1) {
           cout << "\n\t Implicit Operator: + " << endl << endl;
        }
        else if(p!=-1) {
            cout << "\n\t Operator: " << expr.at(p) << endl
                 << "\n\t Position: " << p << endl << endl;
        }

        cout << "\n Expression Prefix... " << endl
             << "\n\t " << syn.toPrefixExpression(expr)   << endl << endl;
    }
    return 0; 
}

